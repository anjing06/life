// pages/search/search.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    hasList:false,
    categoryFilter: false,
    searchStatus: false,
    keyword: '',
    distance:'',
    city:'',
    longitude:'',
    latitude:'',
    tab: [true, true],
    c_id: 0,
    tabTxt: ['全部分类'],
    secondTabs: [
      // { text: '全部分类', id: 0 }, { text: '宴会美食', id: 38 }, { text: '酒店住宿', id: 39 }, { text: '休闲娱乐', id: 40 }, { text: '生活服务', id: 41 }, { text: '汽车服务', id: 42 }, { text: '潮流服饰', id: 43 }, { text: '婚礼用品', id: 44 }, { text: '亲子母婴', id: 45 }, { text: '教育培训', id: 46 },
      ],
    pageNumber: 1,
    pageSize: 10,
    totalPage:0,
    sercherStorage: [],
    StorageFlag: false 

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var self = this
    self.getAllCate()
    self.getGoodsList()
    self.openLocationsercher();
    self.setData({
      city: app.globalData.city,
      longitude: app.globalData.lng,
      latitude: app.globalData.lat,
    })
  },
 
  getAllCate: function () {
    var self = this
    wx.request({
      url: app.url + '/wx/categoryHomeList',
      data: {

      },
      method: 'POST',
      header: {
        'content-type': 'application/json', // 默认值
      },
      success: function (res) {
        console.log(res.data);
        var aa = res.data.unshift({ id: 0, name: '全部分类' })
        self.setData({
          secondTabs: res.data
        })
        console.log(self.data.secondTabs)
      },

    })
  },
  getChild(id,name) {
    var self = this
    wx.request({
      url: app.url + '/wx/getChildCategoryList',
      data: {
        c_id: id
      },
      method: 'get',
      header: {
        'content-type': 'application/json', // 默认值
      },
      success: function (res) {
        console.log(res.data);
        var aa = res.data.unshift({ id: id, name: name, parent_id: 0  })
        var aa = res.data.unshift({ id: 0, name: '全部分类' })
        self.setData({
          secondTabs: res.data
        })
        console.log(self.data.secondTabs)
      },

    })
  },
  inputChange: function (e) {

    this.setData({
      keyword: e.detail.value,
      searchStatus: false
    });
  },
  inputFocus: function () {
    this.setData({
      StorageFlag: true,
      searchStatus: false
    })
  },
  //清除缓存历史
  clearSearchStorage: function () {
    wx.removeStorageSync('searchData')
    this.setData({
      sercherStorage: []
    })
  },
  //打开历史记录列表
  openLocationsercher: function () {
    this.setData({
      sercherStorage: wx.getStorageSync('searchData') || [],
    })
  },
  onKeywordTap: function (e) {
    this.setData({
      keyword: e.target.dataset.keyword,
      StorageFlag: false,
      list: [],
      hasList: false,
      pageNumber: 1
    })
    this.getGoodsList()
  },
  onKeywordConfirm(event) {
    var self = this;
    if (self.data.keyword != '') {
      //将搜索记录更新到缓存
      var datas = wx.getStorageSync('searchData')

      if (datas.indexOf(self.data.keyword) == -1) {
        var searchData = self.data.sercherStorage;
        searchData.push(self.data.keyword)
        wx.setStorageSync('searchData', searchData);
      }
    }
    self.setData({
      list: [],
      hasList: false,
      pageNumber: 1,
    });
    self.getGoodsList()
    self.openLocationsercher()
    self.setData({ StorageFlag: false, })
  },

  getGoodsList: function () {
    wx.showLoading()
    let that = this;
    wx.request({
      url: app.url + '/wx/nearbyShops', //仅为示例，并非真实的接口地址
      data: {
        key: encodeURIComponent(that.data.keyword),
        city: encodeURIComponent(app.globalData.city),
        cat_id: that.data.c_id,
        longitude: app.globalData.lng,
        latitude: app.globalData.lat,
        distance: that.data.distance,
        pageNumber: that.data.pageNumber,
        pageSize: that.data.pageSize,
      },
      method: 'get',
      dataType: 'json',

      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        if (res.data.shops.list.length == 0) {
          that.setData({
            hasList: true,
          })
        } else {
          that.setData({
            list: that.data.list.concat(res.data.shops.list),
            totalPage: res.data.shops.totalPage
          })
        }
        wx.hideLoading()
      }
    })
    // util.request(api.GoodsList, { keyword: that.data.keyword, page: that.data.page, size: that.data.size, sort: that.data.currentSortType, order: that.data.currentSortOrder, categoryId: that.data.categoryId }).then(function (res) {
    // if (res.errno === 0) {
    that.setData({
      searchStatus: true
    });
    // }
    // });
  },
  // 选项卡
  filterTab: function (e) {
    var data = [true, true], index = e.currentTarget.dataset.index;
    data[index] = !this.data.tab[index];
    this.setData({
      tab: data
    })
  },

  //筛选项点击操作
  filter: function (e) {
    var self = this,
      id = e.currentTarget.dataset.id,
      txt = e.currentTarget.dataset.txt,
      level = e.currentTarget.dataset.level, 
      tabTxt = self.data.tabTxt;
    self.setData({
      list: [],
      pageNumber: 1,
    });
    switch (e.currentTarget.dataset.index) {
      case '0':
        tabTxt[0] = txt;
        if (id == 0) {
          self.getAllCate()
          // self.setData({
          //   tab: [false, true],
          //   tabTxt: tabTxt,
          //   c_id: id,
          //   pinpai_txt: txt
          // });
        } else {
          if (level == 0) {
            self.getChild(id, txt)
            // self.setData({
            //   tab: [false, true],
            //   tabTxt: tabTxt,
            //   c_id: id,
            //   pinpai_txt: txt
            // });
          }else{
            // self.setData({
            //   tab: [true, true],
            //   tabTxt: tabTxt,
            //   c_id: id,
            //   pinpai_txt: txt
            // });
          }

        }
        self.setData({
          tab: [true, true],
          tabTxt: tabTxt,
          c_id: id,
          pinpai_txt: txt
        });
        break;
    }
    //数据筛选
    self.getGoodsList();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.longitude != app.globalData.lng){
      this.setData({
        pageNumber: 1,
        list: [],
        keyword: '',
        hasList: false,
        city: app.globalData.city,
        longitude: app.globalData.lng,
        latitude: app.globalData.lat,
      })
      this.getGoodsList()
    }
    

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
this.setData({
      pageNumber:1,
      list:[],
      hasList:false
    })
    this.getGoodsList()
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;

    if (that.data.totalPage > that.data.pageNumber) {
      that.setData({
        pageNumber: that.data.pageNumber + 1,
      })
      that.getGoodsList();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk;
const app = getApp()
Page({
  data: {
    latitude: 0,//地图初次加载时的纬度坐标
    longitude: 0, //地图初次加载时的经度坐标
    name: "" //选择的位置名称
  },
  onLoad: function () {

   var that =this;
    wx.getLocation({
      type: 'gcj02',
      success: function (res) {
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })
      }
    })


    // 实例化API核心类
    qqmapsdk = new QQMapWX({
      key: 'MNXBZ-G5TWD-GYF42-HHZJL-2W2J3-PVBX4'
    });

    that.moveToLocation();
  },
  //移动选点
  moveToLocation: function () {
    var that = this;
    wx.chooseLocation({
      success: function (res) {
        console.log(res.name);
        app.globalData.address = res.name
        app.globalData.lat = res.latitude
        app.globalData.lng = res.longitude
        var city = res.address
        city = city.substring(city.indexOf('省') + 1, city.length)
        city = city.substring(0, city.indexOf('市') + 1)
        app.globalData.city = city
        //选择地点之后返回到原来页面
        wx.navigateBack({
          url: "/pages/appointment/appointment?address=" + res.name + "&lat=" + res.latitude + "&lng=" + res.longitude


        });
      },
      fail: function (err) {
        console.log(err)
      }
    });
  }
});

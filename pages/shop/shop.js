// pages/shop/shop.js
const app = getApp();
import Poster from '../../dist/miniprogram_dist/poster/poster';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    canCall: true,
    shop_id: '',
    ewm: '',
    sowing_img_url: '',
    userid: '',
    is_col: false,
    isFirst:0,
    shop: [],
    visible1: false,
    actions1: [
      {
        name: '生成海报(长按图片保存)',
      },
      {
        name: '去分享',
        icon: 'share',
        openType: 'share'
      }
    ],
    noCollectImage: "/images/collection.png",
    hasCollectImage: "/images/collection-c.png",
    collectBackImage: "/images/collection.png",
    collectText:'',
    jdConfig:{}
  },
  handleOpen1() {
    var that = this
    wx.request({
      url: app.url + '/wx/getWxacode',
      data: {
        shop_id: this.data.shop_id
      },
      method: 'GET',
      header: {
        'content-type': 'application/json', // 默认值
      },
      success: function (res) {
        that.setData({
          ewm: res.data.acode_src,
          sowing_img_url: res.data.sowing_img_url,
        })
      },

    })
    this.setData({
      visible1: true
    });
  },

  handleCancel1() {
    this.setData({
      visible1: false
    });
  },
  handleClickItem1({ detail }) {
    var that = this
    const index = detail.index + 1;
    if(index == 1){
    
      this.onCreatePoster()
    }
  },
  onPosterSuccess(e) {
    const { detail } = e;
    wx.previewImage({
      current: detail,
      urls: [detail]
    })
    wx.downloadFile({
      url: detail,
      success: function (res) {
        console.log(res);
        //图片保存到本地
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success: function (data) {
            wx.showToast({
              title: '保存成功',
              icon: 'success',
              duration: 2000
            })
          },
          fail: function (err) {
            console.log(err);
            if (err.errMsg === "saveImageToPhotosAlbum:fail auth deny") {
              console.log("当初用户拒绝，再次发起授权")
              wx.openSetting({
                success(settingdata) {
                  console.log(settingdata)
                  if (settingdata.authSetting['scope.writePhotosAlbum']) {
                    console.log('获取权限成功，给出再次点击图片保存到相册的提示。')
                  } else {
                    console.log('获取权限失败，给出不给权限就无法正常使用的提示')
                  }
                }
              })
            }
          },
          complete(res) {
            console.log(res);
          }
        })
      }
    })
  },
  onPosterFail(err) {
    console.error(err);
  },
  onCreatePoster() {
     wx.showLoading()
    this.setData({
      jdConfig: {
        width: 750,
        height: 1034,
        backgroundColor: '#fff',
        borderRadius: 30,
        y:30,
        debug: false,
        blocks: [
          {
            width: 690,
            height: 398,
            x: 30,
            y: 183,
            borderWidth: 2,
            borderColor: '#f0c2a0',
            borderRadius: 20,
          },
          {
            width: 634,
            height: 74,
            x: 59,
            y: 770,
            backgroundColor: '#fff',
            opacity: 0.5,
            zIndex: 100,
          },
        ],
        texts: [
          {
            x: 313,
            y: 100,
            baseLine: 'middle',
            text: '优迅生活',
            fontSize: 32,
            color: '#8d8d8d',
          },
          {
            x: 49,
            y: 655,
            baseLine: 'middle',
            text: [
              {
                text: this.data.shop.name,
                fontSize: 32,
                color: '#ec1731',
              }
            ]
          },
          {
            x: 49,
            y: 705,
            baseLine: 'middle',
            width:350,
            lineNum:2,
            text: [
              {
                text: this.data.shop.address,
                fontSize: 24,
                color: '#929292',
              },
            ]
          },
          {
            x: 360,
            y: 825,
            baseLine: 'top',
            text: '长按识别小程序码',
            fontSize: 38,
            color: '#080808',
          },
        ],
        images: [
          {
            width: 634,
            height: 334,
            x: 59,
            y: 210,
            url: this.data.sowing_img_url,
          },
          {
            width: 220,
            height: 220,
            x: 92,
            y: 780,
            url: this.data.ewm,
          }
        ]

      }
    }, () =>{
      Poster.create();
      
    })
    wx.hideLoading()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.setData({
      shop_id: parseInt(options.id)
    })
    wx.showLoading()
    
   
    
    // wx.request({
    //   url: app.url + '/wx/addBrowserRecord',
    //   data: {
    //     shop_id: parseInt(options.id),
    //     userid: app.globalData.userid
    //   },
    //   method: 'GET',
    //   header: {
    //     'content-type': 'application/json', // 默认值
    //   },
    //   success: function (res) {
    //   },

    // })
  },
  getDetail(user,shop){
    var that = this
    
    wx.request({
      url: app.url + '/wx/shopDetails',
      data: {
        shop_id: shop,
        userid: user
      },
      method: 'GET',
      header: {
        'content-type': 'application/json', // 默认值
      },
      success: function (res) {
        console.log(res.data);
        if (res.data.status == 0) {
          if (res.data.shop.is_collection == 0) {
            that.setData({
              collectBackImage: that.data.hasCollectImage,
              is_col: true,
              collectText: '已收藏'
            })
          } else {
            that.setData({
              collectBackImage: that.data.noCollectImage,
              is_col: false,
              collectText: '收藏'
            })
          }
          that.setData({
            shop: res.data.shop,
            
          })
          if(that.data.shop.phone == null){
            that.setData({
              canCall: false
            })
          }else{
            that.setData({
              canCall: true
            })
          }

          wx.hideLoading()
        }


      },

    })
  },
  makePhoneCall(){
    if (this.data.shop.phone != null){
      wx.makePhoneCall({
        phoneNumber: this.data.shop.phone,
      })
    }else{
     return false
    }
    
  },
  nav() {
    var that = this
    // let { latitude2, longitude2, name, desc } = this.data;
    wx.openLocation({
      latitude: that.data.shop.lat,
      longitude: that.data.shop.lng,
      scale: 18,
      name: that.data.shop.name,
      address: that.data.shop.address
    });
  },
  gohome() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  fankui() {
    wx.navigateTo({
      url: '/pages/feedback/feedback?shop_id=' + this.data.shop_id,
    })
  },
  collection(){
    var that = this
    wx.request({
      url: app.url + '/wx/isCollection',
      data: {
        shop_id: that.data.shop_id,
        userid: app.globalData.userid
      },
      method: 'GET',
      header: {
        'content-type': 'application/json', // 默认值
      },
      success: function (res) {
        console.log(res.data);
        if (res.data.status == 0) {
          if (that.data.is_col) {
            that.setData({
              collectBackImage: that.data.noCollectImage,
              is_col: false,
              collectText: '收藏'
            })
          } else {
            that.setData({
              collectBackImage: that.data.hasCollectImage,
              is_col: true,
              collectText: '已收藏'
            })
          }
        }else{
          wx.showToast({
            title: res.data.errMsg,
          })
        }


      },

    })
  },
  bindGetUserInfo: function (e) {
    var that = this;

    if (e.detail.errMsg == "getUserInfo:ok") {

      app.globalData.userInfo = e.detail.userInfo;
      var userInfo = app.globalData.userInfo;
      console.log(userInfo);
      that.setData({
        isFirst: 0
      })


    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    if (app.globalData.userid == '' || app.globalData.userid == undefined) {
      wx.login({
        success: res => {
          console.log("code:" + res.code)
          wx.request({
            url: app.url + '/wx/code2Session', //仅为示例，并非真实的接口地址
            data: {
              code: res.code
            },
            method: 'get',
            dataType: 'json',

            header: {
              'content-type': 'application/json' // 默认值
            },
            success: function (res) {
              app.globalData.userid = res.data.userid
              that.setData({
                userid: app.globalData.userid
              })
              that.getDetail(that.data.userid, that.data.shop_id)
              wx.getSetting({
                success: function (res) {
                  if (res.authSetting['scope.userInfo']) {
                    
                    
                  } else {
                    that.setData({
                      isFirst: 1
                    })
                  }
                },
                fail: function (res) {

                }
              })
            }
          })
        }
      })

    } else {
      that.setData({
        userid: app.globalData.userid,
      })
      this.getDetail(this.data.userid, this.data.shop_id)
    }
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var that = this
    return {
      title: that.data.shop.name,
      desc: that.data.shop.ranges,
      path: '/pages/shop/shop?id=' + that.data.shop_id
    }
  }
})
// pages/like/like.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    hasList: false,
    longitude:'',
    latitude:'',
    city:'',
    pageNumber: 1,
    pageSize: 10,
    totalPage:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAllList()
    this.setData({
      city: app.globalData.city,
      longitude: app.globalData.lng,
      latitude: app.globalData.lat,
    })
  },

getAllList(){
  wx.showLoading()
  var that = this
  wx.request({
    url: app.url + '/wx/hotShops',
    data: {
      pageNumber: that.data.pageNumber,
      pageSize: that.data.pageSize,
      city: encodeURIComponent(app.globalData.city),
      longitude: app.globalData.lng,
      latitude: app.globalData.lat,
    },
    method: 'GET',
    header: {
      'content-type': 'application/json', // 默认值
    },
    success: function (res) {
      console.log(res.data);
      if (res.data.status == 0) {
        if (res.data.hot_shops.list.length == 0) {
          that.setData({
            hasList: true,
          })
        } else {
          that.setData({
            list: that.data.list.concat(res.data.hot_shops.list),
            totalPage: res.data.hot_shops.totalPage
          })
        }

        wx.hideLoading()
      }


    },

  })
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  // onShow: function () {
  //   var that = this
  //   that.setData({
  //     list: [],
  //     hasList: false,
  //     pageNumber: 1,
  //   })
  //   that.getAllList()
  // },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onShow: function () {
    if (this.data.longitude != app.globalData.lng) {
      this.setData({
        pageNumber: 1,
        list: [],
        keyword: '',
        hasList: false,
        city: app.globalData.city,
        longitude: app.globalData.lng,
        latitude: app.globalData.lat,
      })
      this.getAllList()
    }
  },
  onPullDownRefresh: function () {
    var that = this
    that.setData({
      list: [],
      hasList: false,
      pageNumber: 1,
    })
    that.getAllList()
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;

    if (that.data.totalPage > that.data.pageNumber) {
      that.setData({
        pageNumber: that.data.pageNumber + 1,
      })
      that.getAllList();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
// pages/feedback/feedback.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    messageTypes:[],
    id:'',
    types:'',
    index:0,
    content:'',
    shop_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.setData({
      shop_id: options.shop_id,
    })
    wx.request({
      url: app.url + '/wx/messageTypes',
      data: {

      },
      method: 'GET',
      header: {
        'content-type': 'application/json', // 默认值
        // 'cookie': app.globalData.cookieStr
      },
      success: function (res) {
        console.log(res.data);
        if (res.data.status == 0) {
          that.setData({
            messageTypes: res.data.messageTypes,
          })
        }
      },

    })
  },
  goFeedback: function(){
    var that = this
    if (that.data.content == '' || that.data.id == ''){
      wx.showToast({
        icon:'none',
        title: '请填写必要内容',
      })
    }else{
      wx.request({
        url: app.url + '/wx/addUserMessage',
        data: {
          message: encodeURIComponent(that.data.content),
          userid: app.globalData.userid,
          type_id: that.data.id,
          shop_id: that.data.shop_id
        },
        method: 'GET',
        header: {
          'content-type': 'application/json', // 默认值
          // 'cookie': app.globalData.cookieStr
        },
        success: function (res) {
          if(res.data.status == 0){
            wx.showToast({
              title: '反馈成功',
            })
            that.setData({
              id: '',
              types: '',
              content: '',
            })

            wx.navigateBack({
              url: "/pages/shop/shop?shop_id=" + that.data.shop_id + "&userid=" + that.data.userid
            })
          }
        }
      })
    }
    
  },
  bindPickerChange: function (e) {
    var id = this.data.messageTypes[e.detail.value].id
    var types = this.data.messageTypes[e.detail.value].type
    this.setData({
      id: id,
      types: types
    })
  },
  bindKeyInput: function (e) {
    this.setData({
      content: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
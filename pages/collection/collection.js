// pages/like/like.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    hasList:false,
    pageNumber: 1,
    pageSize: 10,
    totalPage: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.getAllList()
    
  },
  gohome() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
getAllList(){
  wx.showLoading()
  var that = this
  wx.request({
    url: app.url + '/wx/collectionShops',
    data: {
      userid: app.globalData.userid
    },
    method: 'GET',
    header: {
      'content-type': 'application/json', // 默认值
    },
    success: function (res) {
      console.log(res.data);
      if (res.data.status == 0) {
        if(res.data.shops.length == 0){
          that.setData({
            hasList: true,
          })
        }else{
          that.setData({
            list: res.data.shops
          })
        }
        

        wx.hideLoading()
      }


    },

  })
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this
    that.setData({
      list: [],
      hasList: false
    })
    that.getAllList()
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
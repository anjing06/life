//index.js
//获取应用实例
let app = getApp();
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk;

Page({
  data: {
    address: '',
    lat: '',
    lng: '',
    imgUrls: [],
    isFirst: 0,
    userInfo:{},
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    list:[],
    navList:[],
    allList:[],
    totalPage:0,
    pageNumber: 1,   // 设置加载的第几次，默认是第一次
    pageSize: 10,      //返回数据的个数
    searchLoading: false, //"上拉加载"的变量，默认false，隐藏
    searchLoadingComplete: false,  //“没有数据”的变量，默认false，隐藏
    moreName:'更多分类'
  },
  //事件处理函数

  onLoad: function (options) {
    wx.showLoading()
    var that = this
    if (app.globalData.lat != undefined){
      that.getShopList()
    }
    
      wx.login({
        success: res => {
          console.log("code:" + res.code)
          wx.request({
            url: app.url + '/wx/code2Session', //仅为示例，并非真实的接口地址
            data: {
              code: res.code
            },
            method: 'get',
            dataType: 'json',

            header: {
              'content-type': 'application/json' // 默认值
            },
            success: function (res) {
              app.globalData.userid = res.data.userid
              wx.getSetting({
                success: function (res) {
                  if (res.authSetting['scope.userInfo']) {

                  } else {
                    that.setData({
                      isFirst: 1
                    })
                  }
                },
                fail: function (res) {

                }
              })
            }
          })
        }
      })
    wx.request({
      url: app.url + '/wx/sowingList',
      data: {

      },
      method: 'POST',
      header: {
        'content-type': 'application/json', // 默认值
      },
      success: function (res) {
        console.log(res.data);
      
          that.setData({
            imgUrls: res.data
          })
        
        wx.hideLoading()

      },

    })
    
    that.getCate()
    
    if (app.globalData.userInfo) {
      that.setData({
        userInfo: app.globalData.userInfo,
        isFirst: 0
      })
    } else if (that.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        that.setData({
          userInfo: res.userInfo,
          isFirst: 0
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          console.log(res.userInfo)
          that.setData({
            userInfo: res.userInfo,
            isFirst: 0
          })
        }
      })
    }
    if (options.lat != undefined) {
      //设置变量 address 的值      
      that.setData({
        address: options.address,
        lat: options.lat,
        lng: options.lng
      });
    }
    else {
      // 实例化API核心类      
      qqmapsdk = new QQMapWX({
        //此key需要用户自己申请        
        key: 'MNXBZ-G5TWD-GYF42-HHZJL-2W2J3-PVBX4'
      });
      var that = this;
      // 调用接口   
      wx.getLocation({
        type: 'gcj02',
        success: function (res) {
          app.globalData.lat = res.latitude
          app.globalData.lng = res.longitude
          that.setData({
            lat: res.latitude,
            lng: res.longitude
          });
          qqmapsdk.reverseGeocoder({
            location: {
              latitude: res.latitude,
              longitude: res.longitude
            },
            success: function (res) {
              
              app.globalData.city = res.result.address_component.city
              that.getShopList()
              that.setData({
                address: res.result.address_component.street,
                // lat: res.result.ad_info.location.lat,
                // lng: res.result.ad_info.location.lng
              });
            },
            fail: function (res) {
              //console.log(res);      
            },
            complete: function (res) {
              //console.log(res);   
            }
          });
        }
      })        
    }
    

  },
  getCate(){
    var that = this
    wx.request({
      url: app.url + '/wx/categoryHomeList',
      data: {

      },
      method: 'POST',
      header: {
        'content-type': 'application/json', // 默认值
      },
      success: function (res) {
        console.log(res.data);

        that.setData({
          navList: res.data.slice(0,9),
          allList: res.data
        })

      },

    })
  },
  getMore(){
    var that = this
    if (that.data.moreName == '更多分类'){
      that.setData({
        navList: that.data.allList,
        moreName: '收起'
      })
    }else{
      that.setData({
        navList: that.data.allList.slice(0, 9),
        moreName: '更多分类'
      })
    }
    
  },
  bindGetUserInfo: function (e) {
    var that = this;

    if (e.detail.errMsg == "getUserInfo:ok") {
      app.globalData.userInfo = e.detail.userInfo;
      wx.request({
        url: app.url + '/wx/userUpdate',
        data: {
          userid:app.globalData.userid,
          userInfo:JSON.stringify(app.globalData.userInfo),
        },
        method: 'POST',
        header: {
          'content-type': 'application/json', // 默认值
        },
        success: function (res) {
        },

      })
     
      var userInfo = app.globalData.userInfo;
      console.log(userInfo);
      that.setData({
        isFirst: 0
      })


    }
  },
  getShopList(){
    wx.showLoading()
    var that = this
    wx.request({
      url: app.url + '/wx/indexShop',
      data: {
        pageNumber: that.data.pageNumber,
        pageSize: that.data.pageSize,
        city: encodeURIComponent(app.globalData.city),
        longitude: app.globalData.lng,
        latitude: app.globalData.lat,
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded', // 默认值
      },
      success: function (res) {
        console.log(res.data);
        if (res.data.status == 0) {
          that.setData({
            list: that.data.list.concat(res.data.shops.list),
            totalPage: res.data.shops.totalPage
          })
          wx.hideLoading()
        }

      },

    })
  },
  onReachBottom: function () {
    let that = this;
    
    if (that.data.totalPage > that.data.pageNumber) {
      that.setData({
        pageNumber: that.data.pageNumber + 1,
      })
      that.getShopList();
    }
},
  /**
 * 生命周期函数--监听页面显示
 */
onShow:function(){
  if (app.globalData.lat != undefined && this.data.lng != app.globalData.lng) {
    this.setData({
      address: app.globalData.address,
      lat: app.globalData.lat,
      lng: app.globalData.lng,
      list: [],
      pageNumber: 1
    });
    this.getShopList()
  }
},
  onPullDownRefresh: function () {
   this.setData({
      address: app.globalData.address,
      lat: app.globalData.lat,
      lng: app.globalData.lng,
      list: [],
      pageNumber: 1
    });
      this.getShopList()
      wx.stopPullDownRefresh()
    }
})
